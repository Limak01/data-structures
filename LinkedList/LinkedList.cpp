#include <iostream>
#include <stdlib.h>
#include "LinkedList.h"

void LinkedList::add(int value) {
    Node* newValue = (Node*)malloc(sizeof(Node));
    newValue->value = value;
    newValue->next = NULL;

    if(this->head == NULL) {
        newValue->prev = NULL;
        head = newValue;
    } else {
        Node* tmp = this->head;

        while(tmp->next != NULL) {
            tmp = tmp->next;
        }

        newValue->prev = tmp;
        tmp->next = newValue;
    }
}

void LinkedList::insert(int value) {
    Node* newValue = (Node*)malloc(sizeof(Node));
    newValue->value = value;
    newValue->prev = NULL;

    if(this->head == NULL) {
        newValue->next = NULL;
        this->head = newValue;
    } else {
        newValue->next = this->head;
        this->head = newValue;
    }
}

void LinkedList::deleteLast() {
    Node* tmp = this->head;
    while(tmp->next != NULL) {
        tmp = tmp->next;
    }

    tmp->prev->next = NULL;
    free(tmp);
}

void LinkedList::deleteFirst() {
    Node* tmp = this->head->next;
    free(this->head);
    this->head = tmp;
}

int LinkedList::size() {
    if(this->head != NULL) {
        int x = 1;
        Node* tmp = this->head;

        while(tmp->next != NULL) {
            x++;
            tmp = tmp->next;
        }

        return x;
    } else {
        return 0;
    }
}

void LinkedList::deleteAt(int index) {
    if(this->head != NULL) {
        if(index == 0)
            deleteFirst();
        else if(index == size()) {
            deleteLast();
        } else {
            int x = 0;
            Node* tmp = this->head;

            while(x != index) {
                x++;
                tmp = tmp->next;
            }

            Node* nextInList = tmp->next;
            Node* prevInList = tmp->prev;

            free(tmp);

            prevInList->next = nextInList;
            nextInList->prev = prevInList;
        }
    }
}

void LinkedList::insertAt(int value, int index) {
    if(this->head != NULL) {
        if(index == 0) {
            insert(value);
        } else if(index == size()) {
            add(value);
        } else {
            Node* newValue = (Node*)malloc(sizeof(Node));
            newValue->value = value;
            int x = 0;
            Node* tmp = this->head;

            while(x != index) {
                x++;
                tmp = tmp->next;
            }

            Node* prevInList = tmp->prev;

            newValue->prev = prevInList;
            newValue->next = tmp;
            prevInList->next = newValue;
            tmp->prev = newValue;
                    
        }
    } else {
        add(value);
    }
}

void LinkedList::print() {
    Node* tmp = this->head;
    while(tmp != NULL)
    {
        printf("Value: %d \n", tmp->value);
        tmp = tmp->next;
    }   
}
