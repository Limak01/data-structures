#pragma once

struct Node {
    int value;
    Node* next;
    Node* prev;
};

class LinkedList {
    private:
        //Pointer to first item of the list
        Node* head;

    public:
        //Add item at the end of the list
        void add(int value);

        //Insert item at beginning of the list
        void insert(int value);

        //Delete last item of the list
        void deleteLast();

        //Delete first item of the list
        void deleteFirst();

        //Return size of the list
        int size();

        //Delete item at given index
        void deleteAt(int index);

        //Insert item at given index
        void insertAt(int value, int index);

        //Print list
        void print();
};
