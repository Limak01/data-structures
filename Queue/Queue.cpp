#include <iostream>
#include <stdlib.h>
#include "Queue.h"


void Queue::add(int value) {
    Node* newValue = (Node*)malloc(sizeof(Node));
    newValue->value = value;
    newValue->next = NULL;

    if(this->first == NULL) {
        first = newValue;
    } else {
        Node* tmp = this->first;

        while(tmp->next != NULL) {
            tmp = tmp->next;
        }

        tmp->next = newValue;
    }
}

int Queue::peek() {
    return this->first->value;
}

int Queue::poll() {
    if(this->first == NULL) {
        return (int)NULL;
    } else {
        Node* tmp = this->first;
        this->first = tmp->next;
        int value = tmp->value;
        free(tmp);

        return value;
    }
}

void Queue::print() {
    if(this->first != NULL) {
        Node* tmp = this->first;

        while(tmp != NULL) {
            printf("Value: %d \n", tmp->value);
            tmp = tmp->next;
        }
    }
}

