#pragma once

struct Node {
    int value;
    Node* next;
};

class Queue {
    //private:
        Node* first;
    
    public:
        //Add new item to Queue
        void add(int value);

        //Return first item in Queue
        int peek();

        //Return first item in Queue and delete it
        int poll();

        //Print Queue
        void print();
};
