#include <iostream>
#include <stdlib.h>
#include "Stack.h"

void Stack::push(int value) {
    Node* newElement = (Node*)malloc(sizeof(Node));
    newElement->value = value;

    if(this->top == NULL) {
        newElement->next = NULL;
        this->top = newElement;
    } else {
        newElement->next = this->top;
        this->top = newElement;
    }
}

int Stack::pop() {
    Node* tmp = this->top;
    this->top = tmp->next;
    int value = tmp->value;

    free(tmp);

    return value;
}

int Stack::peek() {
    return this->top->value;
}

bool Stack::isEmpty() {
    if(this->top == NULL) 
        return true;
    else 
        return false;
}

void Stack::print() {
    Node* tmp = this->top;
    while(tmp != NULL) {
        printf("Value: %d \n", tmp->value);
        tmp = tmp->next;
    }
}