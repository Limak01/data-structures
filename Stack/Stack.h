#pragma once

struct Node {
    int value;
    Node* next;
};

class Stack {
    private:
        Node* top;
    
    public:
        //Add item at the top of stack
        void push(int value);

        //Remove item from top of the stack
        int pop();

        //Return item from top of the stack
        int peek();

        //Checks if stack is empty
        bool isEmpty();

        //Print out the stack
        void print();
};
